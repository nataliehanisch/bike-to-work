package main

import (
	"github.com/kr/pretty"
)

func main() {

	steps := getStepsFromRoute(getRoute())
	wind := getWindSpeedAndDirection()

	pretty.Println(calculateDirectionInDegrees(steps[0], steps[1]))
	pretty.Println(wind)

}
