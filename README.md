# Bike to Work

Ever wonder if it's a good day to bike to work? I wonder every day!

The ultimate goal of this app is to display at a glance whether or not it is a good day to bike to work. Eventually this will integrate Google Maps directions information with the OpenWeatherMaps API to provide information such as:

- How much wind resistance you will experience on your commute (head wind, tail wind, side wind)
- Whether the sun will rise or set during your commute
- If there is likely to be rain/ice/snow or other weather-related hazards
- An estimated difficulty, which will eventually be based on the users own ratings of past cycling difficulty 

## Getting Started

This is just the backend for the project, so all you need is Go and some API keys. 

### Prerequisites

- The Go Programming Language 1.11.5
- A Google Maps API key
- An OpenWeatherMaps API key

### Running the code

After installing the prerequisites, make sure to create a file called `maps-key.txt` and paste your Google Maps API key in it. 

Then just use `go run *.go` to compile and run the code. For now, feel free to edit the content in `main.go` to try out different functions which have been built. Eventually thiscode base will have more of a logical flow, but for now it's just a bunch of scattered pieces.