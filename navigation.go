package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"

	"googlemaps.github.io/maps"
)

type Location struct {
	Latitude  float64
	Longitude float64
	Distance  int
}

// Save a route to file for use later
func saveRoute() {
	origin := "Love Library, Lincoln, NE"
	destination := "A Street Market, Lincoln, NE"

	content, _ := json.Marshal(bikeDirectionsRequest(origin, destination))
	saveRouteToFile(content)
}

// Grab a route from file which had been saved
func getRoute() maps.Route {
	// TODO: Make this get a specific route and not just the first one
	file_names := getRouteFileNames()

	raw, _ := ioutil.ReadFile(file_names[0])

	var data maps.Route
	err := json.Unmarshal([]byte(raw), &data)
	if err != nil {
		// handle error
	}

	return data
}

func getRouteFileNames() []string {
	files, err := filepath.Glob("routes/*")
	if err != nil {
		log.Fatal(err)
	}
	return files
}

// Print the average North/South and East/West for each step on route
func getStepsFromRoute(route maps.Route) []Location {
	steps := make([]Location, len(route.Legs[0].Steps)) // works if there is only one leg to the journey

	for _, leg := range route.Legs {
		for idx, step := range leg.Steps {

			//s2 := s
			s := Location{step.StartLocation.Lat,
				step.StartLocation.Lng,
				step.Distance.Meters}
			// pretty.Println(calculateAverageCardinalDirection(s, s2))
			steps[idx] = s
		}
	}
	return steps
}

// For a starting and ending (lat,lon), calulate the average direction
func calculateDirectionInDegrees(startPoint Location, endPoint Location) float64 {

	var radians = math.Atan2((endPoint.Longitude - startPoint.Longitude), (endPoint.Latitude - startPoint.Latitude))
	var compassReading = radians * (180 / math.Pi)

	return compassReading

}

// Get the bike route for a directions request
func bikeDirectionsRequest(origin string, destination string) maps.Route {

	buf, err := ioutil.ReadFile("maps-key.txt")
	key := string(buf)

	c, err := maps.NewClient(maps.WithAPIKey(key))
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}
	r := &maps.DirectionsRequest{
		Origin:      origin,
		Destination: destination,
		Mode:        "bicycling",
	}
	route, _, err := c.Directions(context.Background(), r)
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}

	return route[0]

}
