package main

import (
	"io/ioutil"
	"time"
)

func saveRouteToFile(content []byte) {
	err := ioutil.WriteFile("routes/route-"+time.Now().Format(time.RFC3339)+".route", content, 0644)
	check(err)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
